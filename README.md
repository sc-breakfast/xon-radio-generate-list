# xon-radio-generate-list

OGG files → PK3 archives + TXT listing

## Installation

```bash
yarn global add xon-radio-generate-list
# or
npm install -g xon-radio-generate-list
```

## Usage

### Options:

```
--help                      Show help                                [boolean]
--version                   Show version number                      [boolean]
--input-songs                                               [array] [required]
--output-list-file                                         [string] [required]
--output-package-directory                                 [string] [required]
--url-prefix                                            [string] [default: ""]
```

### Example

1. You have a directory with music (here named `songs`):
  ```bash
  $ ls songs
  Author — Song 1.ogg
  Author — Song 2.ogg
  Author — Song 3.ogg
  ```

2. You want to make a directory with packages and a listing (here named `radio`):
  ```
  $ mkdir radio
  ```

3. Running the program
  ```bash
  $ xon-radio-generate-list --input-songs 'songs/*.ogg' --output-list-file radio/listing.txt --output-package-directory radio --url-prefix http://jeffs.eu/radio/
  Compressing songs/Author — Song 1.ogg → radio/package1.pk3
  Compressing songs/Author — Song 2.ogg → radio/package2.pk3
  Compressing songs/Author — Song 3.ogg → radio/package3.pk3
  Listing written to radio/listing.txt
  ```

4. Result:
  ```bash
  $ cat radio/listing.txt
  http://jeffs.eu/radio/package1.pk3 music.ogg 152 Author — Song 1
  http://jeffs.eu/radio/package2.pk3 music.ogg 186 Author — Song 2
  http://jeffs.eu/radio/package3.pk3 music.ogg 460 Author — Song 3

  $ ls radio
  package1.pk3
  package2.pk3
  package3.pk3
  listing.txt
  ```
